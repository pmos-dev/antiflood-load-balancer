#!/bin/bash
rm -rf dist; tsc && tslint -p . && rsync -av --delete --exclude="node_modules" --exclude="src" --exclude=".*" --exclude="deploy.sh" --exclude="tslint.json" --exclude="/config" . analytics:/home/mdmysphm/bin/antiflood-load-balancer
