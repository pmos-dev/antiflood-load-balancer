import * as express from 'express';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsRestApi } from 'nodecommons-es-rest';

export class AttemptApi extends CommonsRestApi {
	constructor(
			restServer: CommonsRestServer,
			private path: string,
			attemptCallback: (uid: string) => boolean
	) {
		super(restServer);
		
		super.getHandler(
				`${this.path}attempt/:uid[base62]`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<boolean> => {
					return await attemptCallback(req.strictParams.uid as string);
				}
		);
	}
}
