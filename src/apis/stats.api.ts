import * as express from 'express';

import { commonsArrayUnique, commonsDateDateToYmdHis, commonsDateTruncateToDate } from 'tscommons-es-core';
import { TDateRange } from 'tscommons-es-core';

import { commonsExpressGetQueryDateRange } from 'nodecommons-es-express';
import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsRestApi } from 'nodecommons-es-rest';
import { commonsRestApiBadRequest } from 'nodecommons-es-rest';

import { MinuteModel } from '../models/minute.model';

import { IMinuteTally } from '../interfaces/iminute-tally';

import { TMinuteTally, encodeIMinuteTally } from '../types/tminute-tally';

export class StatsApi extends CommonsRestApi {
	constructor(
			restServer: CommonsRestServer,
			path: string,
			minuteModel: MinuteModel
	) {
		super(restServer);
		
		super.getHandler(
				`${path}stats/available-dates`,
				async (_req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<string[]> => {
					const minuteTallys: IMinuteTally[] = await minuteModel.listAll();

					return commonsArrayUnique(
							minuteTallys
									.map((mt: IMinuteTally): Date => {
										commonsDateTruncateToDate(mt.minute);
										return mt.minute;
									}),
							(_: unknown): _ is Date => true,
							(a: Date, b: Date): boolean => a.getTime() === b.getTime()
					)
							.map((date: Date): string => commonsDateDateToYmdHis(date));
				}
		);
	
		super.getHandler(
				`${path}stats/date-range`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TMinuteTally[]> => {
					const range: TDateRange|undefined = commonsExpressGetQueryDateRange(req);
					if (!range) return commonsRestApiBadRequest('No datetime range supplied');

					const minuteTallys: IMinuteTally[] = await minuteModel.listByDateRange(range);

					return minuteTallys
							.map((minuteTally: IMinuteTally): TMinuteTally => encodeIMinuteTally(minuteTally));
				}
		);
	}
}
