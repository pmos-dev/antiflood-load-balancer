import * as express from 'express';

import { commonsTypeHasProperty, commonsTypeAttemptNumber } from 'tscommons-es-core';

import { ICommonsRequestWithStrictParams } from 'nodecommons-es-express';
import { commonsRestApiBadRequest, CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsRestKeyApi } from 'nodecommons-es-rest';

import { TThresholds } from '../types/tthresholds';

export class AdjustApi extends CommonsRestKeyApi {
	constructor(
			restServer: CommonsRestServer,
			private path: string,
			authKey: string,
			getThresholds: () => TThresholds,
			adjustThresholds: (
					absolutePerSecondThreshold: number,
					perUidPerMinuteThreshold: number
			) => void
	) {
		super(restServer, authKey);
		
		super.getHandler(
				`${this.path}adjust`,
				async (_req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TThresholds> => {
					return getThresholds();
				}
		);
		
		super.patchHandler(
				`${this.path}adjust`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TThresholds> => {
					if (!commonsTypeHasProperty(req.body, 'absolutePerSecondThreshold')) return commonsRestApiBadRequest('No absolutePerSecondThreshold supplied');
					const absolutePerSecondThreshold: number|undefined = commonsTypeAttemptNumber(req.body.absolutePerSecondThreshold);
					if (!absolutePerSecondThreshold) return commonsRestApiBadRequest('Invalid absolutePerSecondThreshold supplied');

					if (!commonsTypeHasProperty(req.body, 'perUidPerMinuteThreshold')) return commonsRestApiBadRequest('No perUidPerMinuteThreshold supplied');
					const perUidPerMinuteThreshold: number|undefined = commonsTypeAttemptNumber(req.body.perUidPerMinuteThreshold);
					if (!perUidPerMinuteThreshold) return commonsRestApiBadRequest('Invalid perUidPerMinuteThreshold supplied');

					adjustThresholds(
							absolutePerSecondThreshold,
							perUidPerMinuteThreshold
					);
					
					return getThresholds();
				}
		);
	}
}
