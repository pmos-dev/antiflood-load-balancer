import {
		commonsOutputDoing,
		commonsOutputSuccess,
		commonsOutputFail
} from 'nodecommons-es-cli';
import { CommonsPostgresService } from 'nodecommons-es-database-postgres';
import {
		CommonsApp,
		assertSet
} from 'nodecommons-es-app';

import { MinuteModel } from '../models/minute.model';

export class AntifloodLoadBalancerCliApp extends CommonsApp {
	private databaseService: CommonsPostgresService|undefined;
	private minuteModel: MinuteModel|undefined;

	constructor() {
		super('antiflood-load-balancer');
	}

	public setDatabaseService(
			databaseService: CommonsPostgresService
	): void {
		this.databaseService = databaseService;
	}

	private async createDb(): Promise<boolean> {
		assertSet(this.minuteModel, 'minuteModel');

		commonsOutputDoing('Creating database');
		
		try {
			await this.minuteModel.createTable();
			
			commonsOutputSuccess();
			return true;
		} catch (e) {
			commonsOutputFail(e.message);
			return false;
		}
	}

	private async dropDb(): Promise<boolean> {
		assertSet(this.minuteModel, 'minuteModel');
		
		commonsOutputDoing('Dropping database');
		
		try {
			await this.minuteModel.dropTable();
			
			commonsOutputSuccess();
			return true;
		} catch (e) {
			commonsOutputFail(e.message);
			return false;
		}
	}

	public async init(): Promise<void> {
		assertSet(this.databaseService, 'databaseService');

		commonsOutputDoing('Connecting to local database');
		await this.databaseService.connect();
		commonsOutputSuccess();

		commonsOutputDoing('Initalising models');
		
		this.minuteModel = new MinuteModel(this.databaseService);
		this.minuteModel.init();
		
		commonsOutputSuccess();

		await super.init();
	}
	
	public async run(): Promise<void> {
		await super.run();
		
		if (this.getArgs().hasAttribute('drop-db')) {
			await this.dropDb();
		}
		if (this.getArgs().hasAttribute('create-db')) {
			await this.createDb();
		}

		this.autoSystemd(
				'/usr/bin/antiflood-load-balancer --daemon',
				'Antiflood Load Balancer'
		);
	}
}
