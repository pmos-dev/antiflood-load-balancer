import {
		commonsOutputDoing,
		commonsOutputSuccess
} from 'nodecommons-es-cli';
import { assertSet } from 'nodecommons-es-app';
import { CommonsPostgresService } from 'nodecommons-es-database-postgres';
import { CommonsRestServer } from 'nodecommons-es-rest';
import { CommonsRestApp } from 'nodecommons-es-app-rest';

import { AttemptApi } from '../apis/attempt.api';
import { StatsApi } from '../apis/stats.api';
import { AdjustApi } from '../apis/adjust.api';

import { Load } from '../classes/load';
import { Logger } from '../classes/logger';

import { MinuteModel } from '../models/minute.model';

import { isTFloodConfig } from '../types/tflood-config';
import { isTLoggerConfig } from '../types/tlogger-config';
import { isTApiConfig } from '../types/tapi-config';
import { TThresholds } from '../types/tthresholds';

export class AntifloodLoadBalancerApp extends CommonsRestApp {
	private databaseService: CommonsPostgresService|undefined;
	private minuteModel: MinuteModel|undefined;

	private load: Load;
	private logger: Logger|undefined;
	private loggerInterval: number;

	private authKey: string;

	constructor() {
		super('antiflood-load-balancer');
		
		const floodConfig: unknown = this.getConfigArea('flood');
		if (!isTFloodConfig(floodConfig)) throw new Error('Invalid flood config');

		this.load = new Load(
				floodConfig.absolutePerSecondThreshold,
				floodConfig.perUidPerMinuteThreshold
		);
	
		const loggerConfig: unknown = this.getConfigArea('logger');
		if (!isTLoggerConfig(loggerConfig)) throw new Error('Invalid logger config');

		this.loggerInterval = loggerConfig.interval;
		
		const apiConfig: unknown = this.getConfigArea('api');
		if (!isTApiConfig(apiConfig)) throw new Error('Invalid API config');
		
		this.authKey = apiConfig.authKey;
	}
	
	public setDatabaseService(
			databaseService: CommonsPostgresService
	): void {
		this.databaseService = databaseService;
	}

	public async init(): Promise<void> {
		assertSet(this.databaseService, 'databaseService');

		commonsOutputDoing('Connecting to local database');
		await this.databaseService.connect();
		commonsOutputSuccess();

		commonsOutputDoing('Initalising models');
		
		this.minuteModel = new MinuteModel(this.databaseService);
		this.minuteModel.init();
		
		commonsOutputSuccess();
		
		this.logger = new Logger(
				this.minuteModel,
				this.load,
				this.loggerInterval
		);

		commonsOutputDoing('Installing APIs');
		
		this.install(
				(restServer: CommonsRestServer, path: string): void => {
					assertSet(this.minuteModel, 'minuteModel');
			
					// tslint:disable-next-line:no-unused-expression
					new AttemptApi(
							restServer,
							path,
							(uid: string): boolean => {
								return this.load.hit(uid);
							}
					);
			
					// tslint:disable-next-line:no-unused-expression
					new AdjustApi(
							restServer,
							path,
							this.authKey,
							(): TThresholds => this.load.getThresholds(),
							(
									absolutePerSecondThreshold: number,
									perUidPerMinuteThreshold: number
							): void => this.load.adjustThresholds(
									absolutePerSecondThreshold,
									perUidPerMinuteThreshold
							)
					);
		
					// tslint:disable-next-line:no-unused-expression
					new StatsApi(
							restServer,
							path,
							this.minuteModel
					);
				}
		);
		
		commonsOutputSuccess();
		await super.init();
	}

	protected listening(): void {
		if (this.logger) this.logger.start();

		super.listening();
	}

	public async shutdown(): Promise<void> {
		if (this.logger) this.logger.stop();

		super.shutdown();
	}
}
