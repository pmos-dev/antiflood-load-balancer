import { TDateRange } from 'tscommons-es-core';
import { ICommonsModel } from 'tscommons-es-models';

import {
		CommonsSqlDatabaseService,
		CommonsDatabaseTypeDateTime,
		CommonsDatabaseTypeInt,
		CommonsDatabaseUniqueKey,
		ECommonsDatabaseTypeSigned,
		ECommonsDatabaseTypeNull
} from 'nodecommons-es-database';
import { CommonsModel } from 'nodecommons-es-models';

import { IMinuteTally } from '../interfaces/iminute-tally';

export interface IInternalMinuteTally extends ICommonsModel, IMinuteTally {}

export class MinuteModel extends CommonsModel<IInternalMinuteTally> {
	constructor(
			database: CommonsSqlDatabaseService
	) {
		super(
				database,
				'minutetallys',
				{
						minute: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
						tally: new CommonsDatabaseTypeInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL),
						unique: new CommonsDatabaseTypeInt(ECommonsDatabaseTypeSigned.UNSIGNED, ECommonsDatabaseTypeNull.NOT_NULL)
				},
				undefined,
				[
						new CommonsDatabaseUniqueKey([ 'minute' ])
				]
		);
	}

	//-------------------------------------------------------------------------
	
	protected preprepare(): void {
		super.preprepare();

		this.database.preprepare(
				'AntiFloodLoadBalancer__Minute__LIST_BY_DATERANGE',
				`
					SELECT ${this.modelFieldCsv(this)}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'minute')} BETWEEN :from AND :to
				`,
				{
						from: this.structure['minute'],
						to: this.structure['minute']
				},
				this.structure
		);
	}
	
	//-------------------------------------------------------------------------
	
	public async listByDateRange(
			dateRange: TDateRange
	): Promise<IInternalMinuteTally[]> {
		return await this.database.executeParams<IInternalMinuteTally>(
				'AntiFloodLoadBalancer__Minute__LIST_BY_DATERANGE',
				{
						from: dateRange.from,
						to: dateRange.to
				}
		 );
	}
}
