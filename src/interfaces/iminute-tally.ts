export interface IMinuteTally {
		minute: Date;
		tally: number;
		unique: number;
}
