import {
		commonsDateDateToYmdHis,
		commonsDateYmdHisToDate
} from 'tscommons-es-core';

export function minuteStamp(date: Date): string {
	return commonsDateDateToYmdHis(date)
			.replace(/[^0-9]/g, '')
			.substring(0, 12);
}

export function unminuteStamp(ms: string): Date|undefined {
	const regex: RegExp = /^([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})$/;
	const result: RegExpMatchArray|null = ms.match(regex);
	if (!result) return undefined;

	const rebuild: string = `${result[1]}-${result[2]}-${result[3]} ${result[4]}:${result[5]}:00`;
	return commonsDateYmdHisToDate(rebuild);
}
