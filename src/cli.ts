#!/usr/bin/env node

//// tslint:disable
//const log = require('why-is-node-running');
//// tslint:enable

import { CommonsConfig } from 'tscommons-es-config';

import {
		commonsOutputSetDebugging,
		commonsOutputSetDaemon,
		commonsOutputDebug,
		commonsOutputDie
} from 'nodecommons-es-cli';
import { CommonsArgs } from 'nodecommons-es-cli';
import { isICommonsCredentials } from 'nodecommons-es-database';
import { CommonsPostgresService } from 'nodecommons-es-database-postgres';

import { AntifloodLoadBalancerApp } from './apps/antiflood-load-balancer.app';
import { AntifloodLoadBalancerCliApp } from './apps/antiflood-load-balancer-cli.app';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) commonsOutputSetDebugging(true);
if (args.hasAttribute('daemon')) commonsOutputSetDaemon(true);

let app: AntifloodLoadBalancerApp|AntifloodLoadBalancerCliApp|undefined;

if (args.hasAttribute('cli')) {
	app = new AntifloodLoadBalancerCliApp();
} else {
	app = new AntifloodLoadBalancerApp();
}

const configAuth: CommonsConfig = app.loadConfigFile('database-auth.json');
const credentials: unknown = configAuth.getObject('database');
if (!isICommonsCredentials(credentials)) commonsOutputDie('Database credentials are not valid');

const databaseService: CommonsPostgresService = new CommonsPostgresService(credentials);
app.setDatabaseService(databaseService);

(async (): Promise<void> => {
	commonsOutputDebug('Starting application: Antiflood Load Balancer');
	await app.start();
	commonsOutputDebug('Application completed');
	
//	setTimeout((): void => {
//		log(); // logs out active handles that are keeping node running
//		process.exit(0);
//	}, 100);
})();
