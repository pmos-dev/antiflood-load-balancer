import { commonsDateDateToYmdHis, commonsDateYmdHisToDate } from 'tscommons-es-core';

import { IMinuteTally } from '../interfaces/iminute-tally';

export type TMinuteTally = Omit<IMinuteTally, 'minute'> & {
		minute: string;
};

export function encodeIMinuteTally(minuteTally: IMinuteTally): TMinuteTally {
	return {
			minute: commonsDateDateToYmdHis(minuteTally.minute),
			tally: minuteTally.tally,
			unique: minuteTally.unique
	};
}

export function decodeIMinuteTally(encoded: TMinuteTally): IMinuteTally {
	return {
			minute: commonsDateYmdHisToDate(encoded.minute),
			tally: encoded.tally,
			unique: encoded.unique
	};
}
