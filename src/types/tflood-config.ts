import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

export type TFloodConfig = {
		absolutePerSecondThreshold: number;
		perUidPerMinuteThreshold: number;
};

export function isTFloodConfig(test: unknown): test is TFloodConfig {
	if (!commonsTypeHasPropertyNumber(test, 'absolutePerSecondThreshold')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'perUidPerMinuteThreshold')) return false;
	
	return true;
}
