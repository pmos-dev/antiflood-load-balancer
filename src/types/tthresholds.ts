export type TThresholds = {
		absolutePerSecondThreshold: number;
		perUidPerMinuteThreshold: number;
};
