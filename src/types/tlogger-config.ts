import { commonsTypeHasPropertyNumber } from 'tscommons-es-core';

export type TLoggerConfig = {
		interval: number;
};

export function isTLoggerConfig(test: unknown): test is TLoggerConfig {
	if (!commonsTypeHasPropertyNumber(test, 'interval')) return false;
	
	return true;
}
