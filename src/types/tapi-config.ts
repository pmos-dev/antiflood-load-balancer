import { commonsBase62HasPropertyId } from 'tscommons-es-core';

export type TApiConfig = {
		authKey: string;
};

export function isTApiConfig(test: unknown): test is TApiConfig {
	if (!commonsBase62HasPropertyId(test, 'authKey')) return false;
	
	return true;
}
