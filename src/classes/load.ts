import { commonsOutputDebug, commonsOutputInfo } from 'nodecommons-es-cli';

import { THit } from '../types/thit';
import { TThresholds } from '../types/tthresholds';

import { TallyMap } from './tally-map';

export class Load {
	private hits: THit[] = [];

	private uidLoad: TallyMap = new TallyMap();

	private tally: number = 0;

	constructor(
			private absolutePerSecondThreshold: number,
			private perUidPerMinuteThreshold: number
	) {
		setInterval(
				(): void => {
					commonsOutputDebug(`Resetting hard tally from ${this.tally}`);
					this.tally = 0;
				},
				1000
		);

		setInterval(
				(): void => {
					commonsOutputDebug(`Resetting uid tallies`);
					this.uidLoad.clear();
				},
				60000
		);
	}

	public getThresholds(): TThresholds {
		return {
				absolutePerSecondThreshold: this.absolutePerSecondThreshold,
				perUidPerMinuteThreshold: this.perUidPerMinuteThreshold
		};
	}

	public adjustThresholds(
			absolutePerSecondThreshold: number,
			perUidPerMinuteThreshold: number
	): void {
		commonsOutputInfo(`Adjusting absolutePerSecondThreshold to ${absolutePerSecondThreshold}`);
		this.absolutePerSecondThreshold = absolutePerSecondThreshold;

		commonsOutputInfo(`Adjusting perUidPerMinuteThreshold to ${perUidPerMinuteThreshold}`);
		this.perUidPerMinuteThreshold = perUidPerMinuteThreshold;
	}

	public hit(uid: string): boolean {
		this.hits.push({
				uid: uid,
				timestamp: new Date()
		});

		this.tally++;

		const uidTally: number = this.uidLoad.increment(uid);

		if (this.tally > this.absolutePerSecondThreshold) return false;
		if (uidTally > this.perUidPerMinuteThreshold) return false;

		return true;
	}

	public listHits(): THit[] {
		return this.hits;
	}

	public resetHits(): void {
		this.hits = [];
	}

	public listAndResetHits(): THit[] {
		const snapshot: THit[] = this.hits.slice();
		this.resetHits();

		return snapshot;
	}
}
