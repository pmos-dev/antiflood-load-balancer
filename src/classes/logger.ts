import { commonsAsyncInterval, commonsAsyncAbortInterval, ECommonsInvocation } from 'tscommons-es-async';

import {
		commonsOutputDoing,
		commonsOutputSuccess,
		commonsOutputFail
} from 'nodecommons-es-cli';

import { MinuteModel } from '../models/minute.model';

import { minuteStamp, unminuteStamp } from '../helpers/minutestamp';

import { IMinuteTally } from '../interfaces/iminute-tally';

import { THit } from '../types/thit';

import { Load } from './load';
import { TallyMap } from './tally-map';

type TThresholdHit = THit & {
		expired: boolean;
};

export class Logger {
	private pending: THit[] = [];

	constructor(
			private minuteModel: MinuteModel,
			private load: Load,
			private interval: number
	) {}

	public start(): void {
		commonsAsyncInterval(
				this.interval,
				ECommonsInvocation.IF,
				async (): Promise<void> => {
					await this.cycle();
				},
				'logger-interval'
		);
	}

	public stop(): void {
		commonsAsyncAbortInterval('logger-interval');
	}

	private async log(hits: THit[]): Promise<void> {
		const minutes: TallyMap = new TallyMap();
		const uids: TallyMap = new TallyMap();

		for (const hit of hits) {
			const ms: string = minuteStamp(hit.timestamp);

			minutes.increment(ms);
			uids.increment(`${ms},${hit.uid}`);
		}

		const uniques: TallyMap = new TallyMap();
		for (const uid of uids.list()) {
			const ms: string = uid.key.split(',')[0];
			uniques.increment(ms);
		}

		const combined: IMinuteTally[] = [];
		for (const minute of minutes.list()) {
			const timestamp: Date|undefined = unminuteStamp(minute.key);
			if (!timestamp) continue;

			combined.push({
					minute: timestamp,
					tally: minute.tally,
					unique: uniques.get(minute.key)
			});
		}

		for (const combine of combined) {
			await this.minuteModel.insert(combine);
		}
	}

	private async logWhenExpired(hits: THit[]): Promise<void> {
		const ms: string = minuteStamp(new Date());

		const thresholds: TThresholdHit[] = [
				...this.pending,
				...hits
		]
				.map((hit: THit): TThresholdHit => {
					const compare: string = minuteStamp(hit.timestamp);

					return {
							...hit,
							expired: compare < ms
					};
				});

		const expired: TThresholdHit[] = thresholds
				.filter((hit: TThresholdHit): boolean => hit.expired);

		this.pending = thresholds
				.filter((hit: TThresholdHit): boolean => !hit.expired);
		
		await this.log(expired);
	}

	private async cycle(): Promise<void> {
		const hits: THit[] = this.load.listAndResetHits();

		try {
			commonsOutputDoing('Running log cycle');
			await this.logWhenExpired(hits);
			commonsOutputSuccess();
		} catch (e) {
			commonsOutputFail(e.message);
		}
	}
}
