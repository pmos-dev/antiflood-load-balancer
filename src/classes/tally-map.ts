type TTally = {
		tally: number;
};

type TTallyMapList = {
		key: string;
		tally: number;
};

export class TallyMap {
	private internal: Map<string, TTally>;

	constructor() {
		this.internal = new Map<string, TTally>();
	}

	public increment(key: string): number {
		if (!this.internal.has(key)) this.internal.set(key, { tally: 0 });

		this.internal.get(key)!.tally++;

		return this.internal.get(key)!.tally;
	}

	public get(key: string): number {
		if (!this.internal.has(key)) return 0;
		return this.internal.get(key)!.tally;
	}

	public clear(): void {
		this.internal.clear();
	}

	public list(): TTallyMapList[] {
		return Array.from(this.internal.keys())
				.map((key: string): TTallyMapList => ({
						key: key,
						tally: this.internal.get(key)!.tally
				}));
	}
}
